% fx.m
%
% A helper function that evaluates the mathematical function that this 
% project requires.
%
% f(x) = 1.0/(x^2 - 3*x + 2)
%
% CSC-301 Project 1
% February 13, 2014
% Andrew Fitzgerald, Ian McBride, Ivan Manolov, and Jorge Yau

function y = fx(x)
    y = 1.0 / (x^2 - 3*x + 2);
end
