% Project1.m
%
% Computes and compares various approximations of the derivative of a
% given mathematic function. Comparisions are made between forward and
% center difference techniques; single and double precision computation;
% and absolute and relative error.
%
% CSC-301 Project 1
% February 13, 2014
% Andrew Fitzgerald, Ian McBride, Ivan Manolov, and Jorge Yau
%
% The given mathematic function and its derivative:
% f(x)  = 1.0 / ((x^2) - 3*x + 2);
% f'(x) = -(2*x - 3)/(x^2 - 3*x + 2)^2

clear all; close all;
num = 40;

% stores the x values of the given function that will be examined
x = [0.5 0.9 0.99];

% allocates memory for variables
errors_forward_single = single(zeros(length(x), num));
errors_center_single = single(zeros(length(x), num)); 
errors_forward_double = zeros(length(x), num);
errors_center_double = zeros(length(x), num);
true_values = zeros(length(x));
h_values = zeros(num, 1);

% stores the h values that be used in the forward/center difference
% calculations (range: 2^0 to 2^-39)
for j=0:num-1
    h_values(j+1) = 1/(2^j);
end

% for each x value
for i = 1:length(x)
    true_values(i) = -(2*x(i) - 3)/(x(i)^2 - 3*x(i) + 2)^2;
    
    % for each h value
    for j=1:num
        % computes double precision forward and center difference
        h = h_values(j);
        dF = (Fx(x(i) + h) - Fx(x(i)))/h;
        dC = (Fx(x(i) + h) - Fx(x(i) - h))/(2*h);
        errors_forward_double(i, j) = abs(true_values(i) - dF);
        errors_center_double(i, j) = abs(true_values(i) - dC);

        % computes single precision forward and center difference
        h = single(h_values(j));
        dF = (Fx(x(i) + h) - Fx(single(x(i))))/h;
        dC = (Fx(x(i) + h) - Fx(x(i) - h))/(2*h);
        errors_forward_single(i, j) = abs(true_values(i) - dF);
        errors_center_single(i, j) = abs(true_values(i) - dC);
    end
    
end


% figure-1: plots the relative error of all the derivative approximations
% using x = 0.5
i = 1;
figure('WindowStyle','docked')
loglog( ...
    h_values, errors_forward_single(i, :) / true_values(i), '-*', ...
    h_values, errors_center_single(i, :) / true_values(i), '-^', ...
    h_values, errors_forward_double(i, :) / true_values(i), '-+', ...
    h_values, errors_center_double(i, :) / true_values(i), '-o')
title('Figure-1 - Relative Error of All Approximations Using X = 0.5')
ylim([1e-12 1e6])
xlabel('H Value')
ylabel('Relative Error')
grid on;
plot_legend = legend(   'Single Forward', 'Single Center', ...
                        'Double Forward', 'Double Center');
set(plot_legend, 'Location', 'SouthEast');

% figure-2: plots a comparison the relative error of the derivative
% approximations using different algorithms while maintaining the same x
% value and precision (x = 0.5 and double precision)
figure('WindowStyle','docked')
loglog( h_values, errors_forward_double(1, :) / true_values(1), '-*', ...
        h_values, errors_center_double(1, :) / true_values(1), '-^')
plot_title = {  'Figure-2 - Relative Error with Different Algorithms' ...
                '(using x = 0.5 and double precision)'};
title(plot_title)
ylim([1e-12 1e6])
xlabel('H Value')
ylabel('Relative Error')
grid on;
plot_legend = legend('Forward Difference', 'Center Difference');
set(plot_legend, 'Location', 'SouthEast');

% figure-3: plots a comparison the relative error of the derivative
% approximations using different precision variables while maintaining the
% same x value and algorithm (x = 0.5 and center difference)
figure('WindowStyle','docked')
loglog( h_values, errors_center_single(1, :) / true_values(1), '-*', ...
        h_values, errors_center_double(1, :) / true_values(1), '-^')
plot_title = {  'Figure-3 - Relative Error with Variables of Different' ...
                'Precision (using x = 0.5 and center difference)'};
title(plot_title)
ylim([1e-12 1e6])
xlabel('H Value')
ylabel('Relative Error')
grid on;
plot_legend = legend('Single Precision', 'Double Precision');
set(plot_legend, 'Location', 'SouthEast');


% figure4: plots a comparison the relative error of the derivative
% approximations using different x values while maintaining the same
% precision and algorithm (double precision and center difference)
figure('WindowStyle','docked')
loglog( h_values, errors_center_double(1, :) / true_values(1), '-*', ...
        h_values, errors_center_double(2, :) / true_values(1), '-^', ...
        h_values, errors_center_double(3, :) / true_values(1), '-+')
plot_title = {  'Figure-4 - Relative Error with Different X Values' ...
                '(using double precision and center difference)'};
title(plot_title)
ylim([1e-12 1e6])
xlabel('H Value')
ylabel('Relative Error')
grid on;
plot_legend = legend('X = 0.5', 'X = 0.9', 'X = 0.99');
set(plot_legend, 'Location', 'SouthEast');

% figure-5: same plot as figure4 but with absolute error
figure('WindowStyle','docked')
loglog( h_values, errors_center_double(1, :) , '-*', ...
        h_values, errors_center_double(2, :), '-^', ...
        h_values, errors_center_double(3, :), '-+')
plot_title = {  'Figure-5 - Absolute Error with Different X Values ' ...
                '(using double precision and center difference)'};
title(plot_title)
ylim([1e-12 1e6])
xlabel('H Value')
ylabel('Absolute Error')
grid on;
plot_legend = legend('X = 0.5', 'X = 0.9', 'X = 0.99');
set(plot_legend, 'Location', 'SouthEast');