## Group assignments from the Spring 2014 Scientific Programming class (CSC-301) ##

#### Reports ####
1. [Project 1][report-1]
1. [Project 2][report-2]
1. [Project 3][report-3]
1. [Project 4][report-4]
1. [Project 5][report-5]

#### Course description: ####
Numerical issues: roundoff error, truncation error, overflow and underflow
errors. Numerical integration; solution of simultaneous equations; curve
fitting. A thorough introduction to scientific programming, using a modern
version of the Fortran or Matlab language. Written reports and oral presenta-
tion of projects.

[report-1]:../../raw/master/project1/report/csc301-project1-report.pdf
[report-2]:../../raw/master/project2/report/csc301-project2-report.pdf
[report-3]:../../raw/master/project3/report/csc301-project3-report.pdf
[report-4]:../../raw/master/project4/report/csc301-project4-report.pdf
[report-5]:../../raw/master/project5/report/csc301-project5-report.pdf
