% Problem2.m 
%
% Solves the differential equation for an forced and undamped spring
% problem using matlab's ode23 function. Force amounts were varied.
%
% CSC-301 Project 4
% May 15, 2014
% Andrew Fitzgerald, Ivan Manolov, Ian McBride, and Jorge Yau

% Setup variables
global F;
tInitial = 0;
tFinal = 100*pi;
uInitial = [ 1; 0];
tSpan = [tInitial tFinal];
fName = 'Forced';
forces = [0.5 1 2];

F = forces(1);
[t1, u1] = ode23(fName, tSpan, uInitial);
F = forces(2);
[t2, u2] = ode23(fName, tSpan, uInitial);
F = forces(3);
[t3, u3] = ode23(fName, tSpan, uInitial);

% Plot solution
figure('WindowStyle','docked')
plot(   t1, u1(:,1), '-', ...
        t2, u2(:,1), '--', ...
        t3, u3(:,1), '-.')
title('Figure 7 - Forced Undamped Solutions')
legend( sprintf('F=%0.0d', forces(1)), ...
        sprintf('F=%0.0d', forces(2)), ...
        sprintf('F=%0.0d', forces(3)))
xlabel('Time')
ylabel('Position')

% Plot orbits
figure('WindowStyle','docked')
plot(   u1(:,1), u1(:,2), '-', ...
        u2(:,1), u2(:,2), '--', ...
        u3(:,1), u3(:,2), '-.')
title('Figure 8 - Forced Undamped Orbits')
legend( sprintf('F=%0.0d', forces(1)), ...
        sprintf('F=%0.0d', forces(2)), ...
        sprintf('F=%0.0d', forces(3)))
xlabel('Position')
ylabel('Velocity')