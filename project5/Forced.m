function fr = forced(t, u)
% Forced.m
%
% Computes the first and second derivatives of the position function for 
% the oscillating weight assuming that:
% K = 1, C = 0, F ~= 0.
%
%   t:  scalar representing time
%   u:  vector of length 2 that satistfies the following equations
%       u(1) = x(t)
%       u(2) = (d/dt)x(t)
%       where x(t) is the position of a oscillating on a spring
%   fr: vector of length 2 that is the derivative of u at time t

global F;
fr = [ u(2);...
       -u(1) + F];