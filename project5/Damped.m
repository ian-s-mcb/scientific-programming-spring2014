function dp = damped(t,u )
% Damped.m
%
% Computes the first and second derivatives of the position function for 
% the oscillating weight assuming that:
% K = 1, C ~= 0, F = 0.
%
%   t:  scalar representing time
%   u:  vector of length 2 that satistfies the following equations
%       u(1) = x(t)
%       u(2) = (d/dt)x(t)
%       where x(t) is the position of a oscillating on a spring
%   dp: vector of length 2 that is the derivative of u at time t

global C;
dp = [ u(2);...
       -u(1)- C*u(2)];