% Problem1.m 
%
% Solves the differential equation for an unforced and undamped spring
% problem using matlab's ode23 function. Error tolerance was varied.
%
% CSC-301 Project 4
% May 15, 2014
% Andrew Fitzgerald, Ivan Manolov, Ian McBride, and Jorge Yau

% Setup variables
tInitial = 0;
tFinal = 100*pi;
uInitial = [ 1; 0];
tSpan = [tInitial tFinal];
fName = 'Unforced';
numOfTolPts = 3;
absTol = logspace(-4, -6, numOfTolPts); % ie. 10^-4, 10^-5, 10^-6

% For each tolerance amount
for i = 1:numOfTolPts

    % Solve differential equation
    options = odeset('AbsTol', absTol(i), 'RelTol', absTol(i)*100);
    [t, u] = ode23(fName, tSpan, uInitial, options);
    
    % Prepare text for plot title
    s1 = sprintf('Figure %d - ', 2*i - 1);
    s2 = sprintf('Figure %d - ', 2*i);
    s3 = sprintf('%0.0d and relTol = %0.0d)', absTol(i), absTol(i)*100);

    % Plot solution
    figure('WindowStyle','docked')
    plot(t, u(:,1))
    title([s1 'Unforced and Undamped Solution (with absTol = ' s3])
    xlabel('Time')
    ylabel('Position')

    % Plot orbits
    figure('WindowStyle','docked')
    plot(u(:,1), u(:,2))
    title([s2 'Unforced and Undamped Orbits (with absTol = ' s3])
    xlabel('Position')
    ylabel('Velocity')
end