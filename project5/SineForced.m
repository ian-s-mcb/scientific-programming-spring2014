function sf = SineForced(t, u)
% SineForced.m
%
% Computes the first and second derivatives of the position function for 
% the oscillating weight assuming that:
% K = 1, C ~= 0, F = cos(W*t).
%
%   t:  scalar representing time
%   u:  vector of length 2 that satistfies the following equations
%       u(1) = x(t)
%       u(2) = (d/dt)x(t)
%       where x(t) is the position of a oscillating on a spring
%   uf: vector of length 2 that is the derivative of u at time t

global W C;
sf = [ u(2);...
       -u(1) - C*u(2) + cos(W*t)];