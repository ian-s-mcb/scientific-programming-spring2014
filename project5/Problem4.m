% Problem4.m 
%
% Solves the differential equation for an sinusoidally forced and damped
% spring problem using matlab's ode23 function. Both the frequency of the
% force function and the damping terms were varied.
%
% CSC-301 Project 4
% May 15, 2014
% Andrew Fitzgerald, Ivan Manolov, Ian McBride, and Jorge Yau

% Setup variables
global W C;
tInitial = 0;
tFinal = 100*pi;
uInitial = [ 1; 0];
tSpan = [tInitial tFinal];
fName = 'SineForced';
freq = linspace(0.5, 1.5, 25);
damping = [0 0.01 0.02 0.03];
amp = zeros(length(damping), length(freq));

% Loop over damping and frequency amounts
for i = 1:length(damping)
    C = damping(i);
    for j = 1:length(freq)
        W = freq(j);
        [t, u] = ode23(fName, tSpan, uInitial);
        amp(i, j) = max(u(:, 1)) - min(u(:, 1));
    end
end

 % Plot amplitude vs frequency
figure('WindowStyle','docked')
plot(   freq, amp(1, :), '-', ...
        freq, amp(2, :), '--', ...
        freq, amp(3, :), ':', ...
        freq, amp(4, :), '-.')
title('Figure 15 - Applying a Sinusoidal Force')
legend('C = 0', 'C = 0.01', 'C = 0.02', 'C = 0.03')
xlabel('Frequency of Applied Force')
ylabel('Amplitude of Resulting Oscillation')