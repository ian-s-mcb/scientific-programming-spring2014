% Problem3.m 
%
% Solves the differential equation for an unforced and damped spring
% problem using matlab's ode23 function. Damping terms were varied.
%
% CSC-301 Project 4
% May 15, 2014
% Andrew Fitzgerald, Ivan Manolov, Ian McBride, and Jorge Yau

% Setup variables
global C;
tInitial = 0;
tFinal = 100*pi;
uInitial = [ 1; 0];
tSpan = [tInitial tFinal];
fName = 'Damped';
damping = [0.01 0.05 0.1];

% For each damping force amount
for i = 1:length(damping)
    C = damping(i);
    [t, u] = ode23(fName, tSpan, uInitial);
    
	% Prepare text for plot title
    s1 = sprintf('Figure %d - ', 2*i + 7);
    s2 = sprintf('Figure %d - ', 2*i + 8);
    s3 = sprintf('(with C = %0.0d)', C);
    
     % Plot solution
    figure('WindowStyle','docked')
    plot(t, u(:,1))
    title([s1 'Unforced and Damped Solution' s3])
    xlabel('Time')
    ylabel('Position')

    % Plot orbits
    figure('WindowStyle','docked')
    plot(u(:,1), u(:,2))
    title([s2 'Unforced and Damped Orbits' s3])
    xlabel('Position')
    ylabel('Velocity')
end