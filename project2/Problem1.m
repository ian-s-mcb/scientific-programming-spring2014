% Problem1.m
%
% Measures the error of the polynomial interpolant of the given function
% below. Then compares the efficiency of the Newton and Vandermonde
% algorithms for this task.
%
% f1(x) = x*(1 - x) + (0.5 * sin(2*pi*x)) + (0.5 * sin(4*pi*x))
%
% CSC-301 Project 2
% March 13, 2014
% Andrew Fitzgerald, Ivan Manolov, Ian McBride, and Jorge Yau

path(path, 'textbook_files');

% computes 100 true values of f1(x)
x0 = linspace(0, 1, 100);
y0 = x0.*(1 - x0) + (0.5 * sin(2*pi*x0)) + (0.5 * sin(4*pi*x0));

% n = degree of interpolant, n + 1 = number of points
n = 5:1:23;
repetitions = 2500;
x = zeros(size(n));
y = zeros(size(n));
c = zeros(size(n));
pVals = zeros(size(n));
error = zeros(size(n));
timeN = zeros(size(n));
timeV = zeros(size(n));

% for each degree of interpolant
for i = 1:length(n)
    
    % compute values to interpolate
    x = linspace(0, 1, n(i) + 1)';
    y = x.*(1 - x) + (0.5 * sin(2*pi*x)) + (0.5 * sin(4*pi*x));

    % compute the newton interpolant
    tic;
    for j = 1:repetitions
        
        c = InterpN2(x, y);
        pVals = HornerN(c, x, x0);
    end
    timeN(i) = toc;
    
    % compute the vandermonde interpolant
    tic;
    for j = 1:repetitions
        
       c = InterpV(x, y);
       pVals = HornerV(c, x0);
    end
    timeV(i) = toc;
    
    % computes magnitude of error vector
    error(i) = sum(sqrt((y0 - pVals).^2));
end

% plots the error vector magnitudes
figure('WindowStyle','docked')
loglog(n, error, '-o')
xlim([min(n), max(n)])
title('Figure 1 - Decreasing Interpolant Error')
xlabel('Degree of Interpolant')
ylabel('Interpolant Error')
set(gca,'XTick', n)

% plots the time for the two algorithms
figure('WindowStyle','docked')
plot(n, timeN, '-o', n, timeV, '-o')
xlim([min(n), max(n)])
title('Figure 2 - Comparing Interpolation Algorithms')
xlabel('Degree of Interpolant')
ylabel(sprintf('Computation Time For %d Repetitions %s', repetitions, ...
    '(in seconds)'))
grid on
legend('Newton', 'Vandermonde', 'Location', 'SouthEast');
set(gca,'XTick', n)