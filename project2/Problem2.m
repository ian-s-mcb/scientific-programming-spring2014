% Problem2.m
%
% Measures the interpolant error of a function that does not interpolate
% well.
%
% CSC-301 Project 2
% March 13, 2014
% Andrew Fitzgerald, Ivan Manolov, Ian McBride, and Jorge Yau
%
% f2(x) = 1.0 /(1.0 + 25*x^2)

path(path, 'textbook_files');

% computes 100 true values of f1(x)
x0 = linspace(-1, 1, 100);
y0 = 1.0 ./ (1.0 + 25*x0.^2);

% n = degree of interpolant, n + 1 = number of points
%n = [5, 11, 17, 23];
n = 5:1:23;
error = zeros(length(n), 1);

% for each degree of interpolant
for i = 1:length(n)
    
    x = linspace(-1, 1, n(i) + 1);
    y = 1.0 ./ (1.0 + 25*x.^2);

    c = InterpN2(x, y);
    pVals = HornerN(c, x, x0);
    
    % computes error norm <<QUESTION IS THIS ABS OR REL ERROR>>
    error(i) = sum(sqrt(((y0 - pVals).^2)));
end

% plots error norm
figure('WindowStyle','docked')
plot(n, error, '-o')
xlim([min(n), max(n)])
plot_title = {'Figure 3 - Limitations of Using High-Ordered' ...
    'Polynomial Interpolants'};
title(plot_title)
xlabel('Degree of Interpolant')
ylabel('Interpolant Error')
grid on
set(gca,'XTick', n)