% Problem4.m
%
% Compares the spline interpolation and least squares approximation of
% partially random data points.
%
% CSC-301 Project 2
% March 13, 2014
% Andrew Fitzgerald, Ivan Manolov, Ian McBride, and Jorge Yau
%
% f3(x) = x*(1 -x)
% f3error(x) = x*(1 -x) + 0.1*random(r)
% random(r) = uniformly distributed random number between (-1, 1)

path(path, 'textbook_files');

% computes 100 true values of f3(x)
x0 = linspace(-1, 1, 100);
y0 = x0.*(1 - x0);

% n = degree of interpolant, n + 1 = number of points
n = 5:1:23;

% creates variables
iError = zeros(size(n));
lsError = zeros(size(n));

% for each degree of interpolant
for i = 1:length(n)
    
    % computes input for approximations based off of f3error(x)
    x = linspace(-1, 1, n(i) + 1);
    re = 2*rand(1, length(x)) - 1;
    y = x.*(1 - x) + 0.1*re;

	% computes the interpolant approximation
    [a, b, c, d] = CubicSpline(x', y');
    iVals = pwCEval(a, b, c, d, x, x0);
    
    % computes the least-squares approximation
    p = polyfit(x, y, 2);
    lsVals = polyval(p, x0);
    
    % computes the approximation error
    iError(i) = sum(sqrt(((y0 - iVals').^2)));
    lsError(i) = sum(sqrt(((y0 - lsVals).^2)));
end

% plots error for each approximation
figure('WindowStyle','docked')
plot(n, iError, '-o', n, lsError, '-o')
plot_title = {'Figure 5 - Error When Approximating With Partially ' ...
    'Random Data Points'};
title(plot_title)
xlabel('Number of Data Points')
ylabel('Error')
legend('Spline Interpolant', 'Least-squares');
grid on