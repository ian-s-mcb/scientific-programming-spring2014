% Problem3.m
%
% Measures the cubic spline interpolant error of the function from 
% Problem2.m
%
% CSC-301 Project 2
% March 13, 2014
% Andrew Fitzgerald, Ivan Manolov, Ian McBride, and Jorge Yau
%
% f2(x) = 1.0 /(1.0 + 25*x^2)

path(path, 'textbook_files');

% computes 100 true values of f1(x)
x0 = linspace(-1, 1, 100)';
y0 = 1.0 ./ (1.0 + 25*x0.^2);
%n = [5, 11, 17, 23];
n = 5:1:23;
error = zeros(length(n), 1);

for i = 1:length(n)
    
    x = linspace(-1, 1, n(i) + 1)';
    y = 1.0 ./ (1.0 + 25*x.^2);
    
    [a, b, c, d] = CubicSpline(x, y);
    s = pwCEval(a, b, c, d, x, x0);
    
    % computes error norm <<QUESTION IS THIS ABS OR REL ERROR>>
    error(i) = sum(sqrt(((y0 - s).^2)));
end

figure('WindowStyle','docked')
loglog(n, error, '-o')
xlim([min(n), max(n)])
title('Figure 4 - Spline Interpolant Error')
xlabel('Degree of Interpolant')
ylabel('Interpolant Error')
set(gca,'XTick', n)