% Project4.m
%
% Numerical solutions to the mesh differential equation problem.
%
% CSC-301 Project 4
% May 1, 2014
% Andrew Fitzgerald, Ivan Manolov, Ian McBride, and Jorge Yau

clear all; close all; clc;

SurfPlot;
TimePlot;
ErrorPlot;