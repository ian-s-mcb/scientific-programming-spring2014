function [solution, time] = ApplyPoisson(N, loadFunc)
% ApplyPoisson.m
%
% Apply Poisson Solver to the mesh differential equation problem
% using one of the two different load functions.
%
% N: Integer of size of mesh
% loadFunc: An integer (of value 1 or 2) that indicates which load function
%           to use
%
% CSC-301 Project 4
% May 1, 2014
% Andrew Fitzgerald, Ivan Manolov, Ian McBride, and Jorge Yau

tic;

% Parameters
n = N - 1;

if (loadFunc == 1)
    b = GenerateB1(N);
else
    b = GenerateB2(N);
end

solution = poicalc(-b, n, n);

% Reshape solution
solution = reshape(solution, n, n);

time = toc;