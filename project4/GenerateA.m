function A = GenerateA(N)
% GenerateA.m
%
% Generates matrix A for the mesh differential equation problem.
% N: Integer of size of mesh
%
% CSC-301 Project 4
% May 1, 2014
% Andrew Fitzgerald, Ivan Manolov, Ian McBride, and Jorge Yau

n = N-1;
meshSize = n*n;
 
% Build matrix D
D_block = triu(tril(ones(n), 1), -1) - 5*eye(n);
D_repeat = repmat({D_block}, n, 1);
D_matrix = blkdiag(D_repeat{:});
 
% Setup Identity matrix
I_vector = ones(meshSize-n, 1)';
I_lower = diag(I_vector, n);
I_upper = diag(I_vector, -n);
 
A = D_matrix + I_lower + I_upper;
