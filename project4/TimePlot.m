% TimePlot.m
%
% Plots the computation times for the three approximations using different
% mesh sizes.
%
% CSC-301 Project 4
% May 1, 2014
% Andrew Fitzgerald, Ivan Manolov, Ian McBride, and Jorge Yau

% Calculate computation time for small mesh sizes
meshSizes1 = [8 16 32 64];
methods = 3; % number of approximation methods
times1 = zeros(methods, length(meshSizes1));
for i = 1:length(meshSizes1)
    N = meshSizes1(i);
    [~, times1(1, i)] = ApplyFull(N, 1);
    [~, times1(2, i)] = ApplySparse(N, 1);
    [~, times1(3, i)] = ApplyPoisson(N, 1);
end

% Calculate computation time for large mesh sizes
methods = 2;
meshSizes2 = [64 128 256];
times2 = zeros(methods, length(meshSizes2));
for i = 1:length(meshSizes2)
    N = meshSizes2(i);
    [~, times2(1, i)] = ApplySparse(N, 1);
    [~, times2(2, i)] = ApplyPoisson(N, 1);
end

% Plots computation time for all mesh sizes
figure('WindowStyle', 'docked')
plot(   meshSizes1, times1(1, :), 'o-', ...
        [meshSizes1 meshSizes2], [times1(2, :) times2(1, :)], 'o--', ...
        [meshSizes1 meshSizes2], [times1(3, :) times2(2, :)], 'o:')
title('Efficiency of Approximations (using 1st load function)')
xlabel('Mesh Size (N)')
ylabel('Computation Time (seconds)')
plot_legend = legend('Full', 'Sparse', 'Poisson');
set(plot_legend, 'Location', 'Best');