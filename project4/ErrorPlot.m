% ErrorPlot.m
%
% Plots the error norm of load functions
%
% CSC-301 Project 4
% May 1, 2014
% Andrew Fitzgerald, Ivan Manolov, Ian McBride, and Jorge Yau

meshSizes = [8 16 32 64];
len = length(meshSizes);
errorVals = zeros(1,len );
for i= 1:len
    N = meshSizes(i);
    real = AnalyticalSolution(N, 1);
    [approx, time] = ApplyFull(N, 1);
    errorVals(i) = CalcError(approx, real);
end

figure('WindowStyle','docked')
plot(meshSizes, errorVals, 'o-')
xlabel('Mesh Size (N)');
ylabel('Error Norm');
plot_title = 'Error norm between numerical and analytical techniques';
title(plot_title)