function solution = AnalyticalSolution(N, loadFunc)
% AnalyticalSolution.m
%
% Computes the analytical solution to the mesh differential equation
% problem using one of the two different load functions.
%
% N: Integer of size of mesh
% loadFunc: An integer (of value 1 or 2) that indicates which load function
%           to use
%
% CSC-301 Project 4
% May 1, 2014
% Andrew Fitzgerald, Ivan Manolov, Ian McBride, and Jorge Yau

n = N - 1;
h = 1 / N;
solution = zeros(n);
interval = (0 : h : 1);
interval = interval(2:end-1);

if loadFunc == 1
    for i = 1:length(interval)
        for j = 1:length(interval)
            x = interval(i);
            y = interval(j);
            solution(i, j) = (-1 / (pi^2)) * ( ...
                0.5 * sin(pi * x) * sin(pi * y) + ...
                (3 / 13) * sin(3 * pi * x) * sin(2 * pi * y) + ...
                (3 / 13) * sin(2 * pi * x) * sin(3 * pi * y));
        end
    end
else
    % code for 2nd load function
end