function A = GenerateSparseA(N)
% GenerateSparseA.m
%
% Generates a sparse matrix A for the mesh differential equation problem.
% N: Integer of size of mesh
%
% CSC-301 Project 4
% May 1, 2014
% Andrew Fitzgerald, Ivan Manolov, Ian McBride, and Jorge Yau

% Generates -4 diagonal
n = N-1;
meshSize = n * n;
D = sparse(1:meshSize, 1:meshSize, -4 * ones(1, meshSize), meshSize, ...
    meshSize);

% Generates band of 1s
index = 1;
bandSize = meshSize - n;
E1 = zeros(1, bandSize);
E2 = zeros(1, bandSize);
for i = 1:meshSize
    if(mod(i, n) ~= 0)
        E1(index) = i + 1;
        E2(index) = i;
        index = index + 1;
    end
end
E = sparse(E1, E2, ones(1, bandSize), meshSize, meshSize);

% Generate identity matrix
I = sparse(N:meshSize, 1:meshSize - n, ones(1, meshSize - n), meshSize, ...
    meshSize);

A = E + D + I + E' + I';