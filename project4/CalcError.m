function Error = CalcError(approx, real)
% calcError.m
%
% This function takes the analytical and numerical solutions for 
% load functions and calculates their error
%
% approx: numerical solution
% real: analytical solution
%
% CSC-301 Project 4
% May 1, 2014
% Andrew Fitzgerald, Ivan Manolov, Ian McBride, and Jorge Yau

sum = 0;
n = length(real);
for i = 1:n
    for j = 1:n
        diff = (real(i,j) - approx(i,j))^2;
        sum = sum + diff;
    end
end

Error = sqrt((1/(n^2))*sum);