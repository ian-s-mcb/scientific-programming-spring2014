function b2 = GenerateB2(N)
% GenerateB2.m
%
% Generates a row-wise vector b2 for the mesh differential equation problem
% using the 2nd load function. 
%
% N: Integer of size of mesh
%
% CSC-301 Project 4
% May 1, 2014
% Andrew Fitzgerald, Ivan Manolov, Ian McBride, and Jorge Yau

% Parameters
n = N - 1;
h = 1 / N;

% Allocate matrix b
b2 = zeros(n);

% Generate matrix b
interval = (0 : h : 1);
interval = interval(2:end-1);
for i = 1 : length(interval)
    for j = 1 : length(interval)
        x = interval(i);
        y = interval(j);
        if (x == 0.5) && (y == 0.25)
            b2(i, j) = (h^-2);
        elseif (x == 0.5) && (y == 0.75)
            b2(i, j) = (h^-2);
        end
    end
end

% Turn matrix b into row-wise vector
b2 = b2(:);

b2 = b2/(N^2);