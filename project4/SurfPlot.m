% SurfPlot.m
%
% Plots the surfaces produced by the partial differentiation approximations
% when using both the first and second load function.
%
% CSC-301 Project 4
% May 1, 2014
% Andrew Fitzgerald, Ivan Manolov, Ian McBride, and Jorge Yau

figure('WindowStyle','docked')
surfc(reshape(GenerateB1(64), 63, 63));
title('1st Load Function (using N = 64)')
xlabel('X')
ylabel('Y')
zlabel('Z')
shading interp
colormap(gray)

figure('WindowStyle','docked')
surfc(ApplyFull(16, 1));
title('Solution Surface (using 1st load function and N = 16)')
xlabel('X')
ylabel('Y')
zlabel('Z')
shading interp
colormap(gray)

figure('WindowStyle','docked')
surfc(ApplySparse(64, 1));
title('Solution Surface (using 1st load function and N = 64)')
xlabel('X')
ylabel('Y')
zlabel('Z')
shading interp
colormap(gray)

figure('WindowStyle','docked')
surfc(reshape(GenerateB2(64), 63, 63));
title('2nd Load Function (using N = 64)')
xlabel('X')
ylabel('Y')
zlabel('Z')
shading interp
colormap(gray)

figure('WindowStyle','docked')
surfc(ApplyFull(16, 2));
title('Solution Surface (using 2nd load function and N = 16)')
xlabel('X')
ylabel('Y')
zlabel('Z')
shading interp
colormap(gray)

figure('WindowStyle','docked')
surfc(ApplySparse(64, 2));
title('Solution Surface (using 2nd load function and N = 64)')
xlabel('X')
ylabel('Y')
zlabel('Z')
shading interp
colormap(gray)