function b1 = GenerateB1(N)
% GenerateB1.m
%
% Generates a row-wise vector b1 for the mesh differential equation problem
% using the 1st load function. 
%
% N: Integer of size of mesh
%
% CSC-301 Project 4
% May 1, 2014
% Andrew Fitzgerald, Ivan Manolov, Ian McBride, and Jorge Yau

% Parameters
n = N - 1;
h = 1 / N;

% Allocate matrix b
b1 = zeros(n);

% Generate matrix b
interval = (0 : h : 1);
interval = interval(2:end-1);
for i = 1 : length(interval)
    for j = 1 : length(interval)
        x = interval(i);
        y = interval(j);
        b1(i, j) =  sin(pi*x)*sin(pi*y) + ...
                    3*sin(3*pi*x)*sin(2*pi*y) + ...
                    3*sin(2*pi*x)*sin(3*pi*y);
    end
end

% Turn matrix b into row-wise vector
b1 = b1(:);

b1 = b1/(N^2);