% Problem1.m
%
% Given density function f(x) = Ce^((-x^2)/2), which is the normal 
% distribution, this code estimates the value of C by taking the integral
% from -inf to inf of f(x). Integration is performed using the Composite
% Newton-Cotes method. The error of integral estimate is restricted to at
% most 10^-8 in magnitude.
%
% This code indicates that c = 0.39894228.
%
% CSC-301 Project 3
% April 3, 2014
% Andrew Fitzgerald, Ian McBride, and Jorge Yau

path(path, 'textbook_files');

a = 0;
b = 1:10;
m = 3;
n = 10;
c = zeros(length(b), 1);
prev = -Inf;
for i = 1:length(b)
    integral = 2*CompQNC('dens', a, b(i), m, n);
    c(i) = 1 / integral;
    if (abs(c(i) - prev) < 1e-8)
        break;
    else
        prev = c(i);
    end
end
format long
[b' c]