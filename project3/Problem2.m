% Problem2.m
%
% Computes the distribution function of the normal distribution using the c
% value from Problem1. Cubic splines are used to cut down on the number 
% integration operations.
%
% As seen in Problem 1, c = 0.39894228.
%
% CSC-301 Project 3
% April 3, 2014
% Andrew Fitzgerald, Ian McBride, and Jorge Yau

path(path, 'textbook_files');

xVals = linspace(0, 3.5, 10)';
prob = zeros(size(xVals));
a = -7;
m = 3;
n = 30;
for i = 1:length(xVals)
    prob(i) = CompQNC('densWithC', a, xVals(i), m, n);
end

moreXVals = linspace(0, 3.5, 100);
[a1, b1, c1, d1] = CubicSpline(xVals, prob);
moreProb = pwCEval(a1, b1, c1, d1, xVals, moreXVals);

figure('WindowStyle','docked')
plot(moreXVals, moreProb, '.')
plot_title = ['Figure 1 - Distribution Function for the ' ...
    'Normal Distribution'];
title(plot_title)
xlabel('x')
ylabel('Distribution Function');
axis([0 3.5 0 1.1]);
grid on;