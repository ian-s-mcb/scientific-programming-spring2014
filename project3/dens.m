function y = dens(x)

y = exp((-x.^2)/2);